<?php
/**
 * Copyright (c) 2015-2015 Oddny AB
 *
 * Oddny reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of Oddny, except as provided by licence. A licence
 * under Oddny's rights in the Program may be available directly from
 * Oddny.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    Oddny
 * @package     Oddny_Module
 * @copyright   Copyright (c) 2015-2015 Oddny AB
 */

namespace Oddny\PyramidConnector\Model\Export;

class Order extends \Oddny\PyramidConnector\Model\Integration
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Oddny\PyramidConnector\Model\ApiFactory
     */
    protected $pyramidConnectorApiFactory;

    /**
     * @var \Oddny\PyramidConnector\Helper\Data
     */
    protected $pyramidConnectorHelper;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    protected $orderCollectionFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    protected $_countryFactory;



    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Oddny\PyramidConnector\Model\ApiFactory $pyramidConnectorApiFactory,
        \Psr\Log\LoggerInterface $logger,
        \Oddny\PyramidConnector\Helper\Data $pyramidConnectorHelper,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Directory\Model\CountryFactory $countryFactory
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->pyramidConnectorApiFactory = $pyramidConnectorApiFactory;
        $this->logger = $logger;
        $this->pyramidConnectorHelper = $pyramidConnectorHelper;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_countryFactory = $countryFactory;
    }

    protected function getIsEnabled()
    {
        $enabled = $this->scopeConfig->getValue('pyramidconnector/general/oddny_order_export_enabled', \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE);

        return $enabled;
    }

    public function run($manualOrders = null)
    {
        if (!$this->getIsEnabled()) {
            return;
        }

        if (is_array($manualOrders) && !empty($manualOrders)) {
            $orders = Mage::getModel('sales/order')
                        ->addFieldToFilter('increment_id', array('in' => $manualOrders));
        }else{

            $orders = $this->getOrdersToExport();
        }

        $ordersExportedCount = 0;

        if (count($orders) > 0) {
            $this->logger->log(\Monolog\Logger::ERROR, 'Starting order export');
        }

        foreach ($orders as $order) {
            $orderData = $this->getOrderData($order);

            $result = $this->pyramidConnectorApiFactory->create()->exportOrder($orderData);
            if ($result['success'] === true) {
                $order->setExported(1);
                $order->setExternalOrderId($result['externalOrderId']);
                $order->save();
                $ordersExportedCount++;
                $this->logger->log(\Monolog\Logger::NOTICE, 'Exported: '. $order->getIncrementId(). ' External order id: ' . $result['externalOrderId']);
            }else {
                $this->logger->log(\Monolog\Logger::ERROR, 'failed order export: '. $order->getIncrementId());
                if (isset($result['errorMsg'])) {
                    $this->logger->log(\Monolog\Logger::ERROR, 'Pyramid error: ' . $result['errorMsg'] . ' - Order increment_id: ' . $order->getIncrementId());
                    print_r('Pyramid error: ' . $result['errorMsg'] . ' - Order increment_id: ' . $order->getIncrementId() . "\n");
                }

            }

        }

        if (count($orders) > 0 && $ordersExportedCount > 0) {
            $this->logger->log(\Monolog\Logger::ERROR, 'finished order export: '.$ordersExportedCount.' orders');
        }

        print_r("\n");
        print_r('Orders exported: ' . $ordersExportedCount . "\n");


    }

    public function getOrdersToExport()
    {
        $orderCollecion = $this->orderCollectionFactory
            ->create()
            ->addFieldToSelect('*');

        $orderCollecion->addFieldToFilter('exported', ['null' => true]);
        $orderCollecion->addFieldToFilter('status', ['eq' => 'processing']);
        $orderCollecion->addFieldToFilter('store_id', ['neq' => 11]);

        return $orderCollecion;
    }

    public function getOrderData($order = null)
    {
        $billingAddress = $order->getBillingAddress();
        $shippingAddress = $order->getShippingAddress() ? $order->getShippingAddress() : $order->getBillingAddress();

        $orderObject = [
                'orderMessage'              => '',
                'vatcodeID'                 => 1,
                'currencycodeID'            => $order->getOrderCurrencyCode(),
                'countryID'                 => $shippingAddress->getCountryId(),
                'paymentID'                 => $this->getPaymentId($order),
                'deliverytypeID'            => $this->getShippingCode($order),
                'magentoOrderID'            => $order->getIncrementId(),
                'customerName'              => $shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname(),
                'customerStreetAddress'     => $shippingAddress->getStreetLine(1) . ' ' . $shippingAddress->getStreetLine(2),
                'customerStreetPostalNo'    => $shippingAddress->getPostcode(),
                'customerStreetCity'        => $shippingAddress->getCity(),
                /*'customerCountryName'       => $this->getCountryName($shippingAddress->getCountryId()),*/
                'customerCountryState'      => $shippingAddress->getRegionCode(),
                'customerCellPhone'         => $shippingAddress->getTelephone(),
                'customerEMail'             => $shippingAddress->getEmail(),
                'WebOrderRows'              => $this->getOrderLines($order)
        ];

        $orderObject['WebOrderRows']['WebOrderRow'][] = $this->getShippingRow($order);

        return $orderObject;
    }

    protected function getOrderLines($order)
    {
        $orderlines = [];
        foreach ($order->getAllVisibleItems() as $orderitem) {


            $orderlines[] = [
                'productID' => $orderitem->getSku(),
                'productPriceNoVat' => (($orderitem->getRowTotalInclTax() - $orderitem->getDiscountAmount()) - $orderitem->getTaxAmount()) / $orderitem->getQtyOrdered(),
                'productQty' => $orderitem->getQtyOrdered(),
            ];
        }

        return ['WebOrderRow' => $orderlines];

    }

    private function getShippingRow($order)
    {

        $shippingRow = [
            'productID' => 'SHIPPING',
            'productPriceNoVat' => ($order->getShippingInclTax() - $order->getShippingTaxAmount()),
            'productQty' => 1,
        ];

        return $shippingRow;
    }

    private function getShippingCode($order)
    {
        return 'P';
    }

    protected function getPaymentId($order)
    {
        $payment = $order->getPayment();

        return $payment->getLastTransId();
        /*
        $method = strtolower($payment->getMethod());
        if (strpos($method, 'klarna') !== false) {
            return 'K';
        }

        if (strpos($method, 'paypal') !== false) {
            return 'P';
        }

        return 'A';*/
    }

    protected function getCountryName($countryCode)
    {
        $country = $this->_countryFactory->create()->loadByCode($countryCode);
        return $country->getName();
    }

}
