<?php
/**
 * Copyright (c) 2015-2015 Oddny AB
 *
 * Oddny reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of Oddny, except as provided by licence. A licence
 * under Oddny's rights in the Program may be available directly from
 * Oddny.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    Oddny
 * @package     Oddny_Module
 * @copyright   Copyright (c) 2015-2015 Oddny AB
 */
namespace Oddny\PyramidConnector\Model\Import;

use \Magento\Framework\App\ObjectManager;

class Shipment extends \Oddny\PyramidConnector\Model\Integration
{

    const SHIPMENT_ROW_TYPE_HEAD = 'H';
    const SHIPMENT_ROW_TYPE_ROW = 'L';

    /**
     * @var \Oddny\PyramidConnector\Model\ApiFactory
     */
    protected $pyramidConnectorApiFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var \Magento\Sales\Model\Service\InvoiceService
     */
    protected $_invoiceService;

    /**
     * @var \Magento\Sales\Model\Order\InvoiceRepository
     */
    protected $invoiceRepository;


    /**
     * @var \Magento\Sales\Model\Service\ShipmentService
     */
    protected $_shipmentService;

    /**
     * @var \Magento\Framework\DB\Transaction
     */
    protected $_transaction;

    /**
     * The ShipmentFactory is used to create a new Shipment.
     *
     * @var \Magento\Sales\Model\Order\ShipmentFactory
     */
    protected $shipmentFactory;
    /**
     * The ShipmentRepository is used to load, save and delete shipments.
     *
     * @var \Magento\Sales\Model\Order\ShipmentRepository
     */
    protected $shipmentRepository;
    /**
     * The ShipmentNotifier class is used to send a notification email to the customer.
     *
     * @var ShipmentNotifier
     */
    protected $shipmentNotifier;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var ObjectManager
     */
    protected $objectManager;


    public function __construct(
        \Oddny\PyramidConnector\Model\ApiFactory $pyramidConnectorApiFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Model\Order\InvoiceRepository $invoiceRepository,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Sales\Model\Order\ShipmentRepository $shipmentRepository,
        \Magento\Sales\Model\Order\ShipmentFactory $shipmentFactory,
        \Magento\Shipping\Model\ShipmentNotifier $shipmentNotifier,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory
    ) {
        $this->pyramidConnectorApiFactory = $pyramidConnectorApiFactory;
        $this->logger = $logger;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->_orderRepository = $orderRepository;
        $this->_invoiceService = $invoiceService;
        $this->invoiceRepository = $invoiceRepository;
        $this->_transaction = $transaction;
        $this->shipmentFactory = $shipmentFactory;
        $this->shipmentRepository = $shipmentRepository;
        $this->shipmentNotifier = $shipmentNotifier;
        $this->objectManager = ObjectManager::getInstance();
        $this->scopeConfig = $scopeConfig;
        $this->_trackFactory = $trackFactory;

    }

    protected function getIsEnabled()
    {
        $enabled = $this->scopeConfig->getValue('pyramidconnector/general/oddny_shipment_import_enabled', \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE);

        return $enabled;
    }

    protected function invoiceViaApi()
    {
        return $this->scopeConfig->getValue('pyramidconnector/general/create_invoice_with_api', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

    }

    public function run()
    {
        if (!$this->getIsEnabled()) {
            return;
        }

        $orderIncrementIds = array();

        $orders = $this->getOrdersToShip();
        print_r('orders to fetch shipment for: ' . count($orders));
        /*foreach ($orders as $order) {
            $orderIncrementIds[] = $order->getIncrementId();
        }*/


        foreach ($orders as $order) {

            $shipments = $this->pyramidConnectorApiFactory->create()->getShipment($order->getExternalOrderId()); //do api request to fetch shipment
            foreach ($shipments as $shipment) {
                $success = true;

                $shipmentDataList = array();


                $result = $this->importShipment($order, $shipment);
                if ($result[0] == false) {
                    $this->logger->log(\Monolog\Logger::NOTICE, 'Import failed shipment for order: ' . $order->getIncrementId() . ' external order id: ' . $order->getExternalOrderId());
                    $this->logger->log(\Monolog\Logger::ERROR, $result[1]);
                    print_r('Import failed shipment for order: ' . $order->getIncrementId() . ' external order id: ' . $order->getExternalOrderId() . "\n");
                    print_r($result);
                    $success = false;
                } else {
                    $this->logger->log(\Monolog\Logger::NOTICE, 'Import success shipment for order: ' . $order->getIncrementId() . ' external order id: ' . $order->getExternalOrderId());
                    print_r('Import success shipment for order: ' . $order->getIncrementId() . ' external order id: ' . $order->getExternalOrderId() . "\n");
                }
            }

        }
    }


    protected function getOrdersToShip()
    {
        $orderCollecion = $this->orderCollectionFactory
            ->create()
            ->addFieldToSelect('*');

        $orderCollecion->addFieldToFilter('exported', ['eq' => 1]);
        $orderCollecion->addFieldToFilter('shipped', ['null' => true]);
        $orderCollecion->addFieldToFilter('status', ['eq' => 'processing']);
        $orderCollecion->addFieldToFilter('store_id', ['neq' => 11]);

        return $orderCollecion;
    }

    public function importShipment($order, $shipmentData)
    {
        $orderNo = $order->getIncrementId();
        $orderLines = [];



        try{
            $qtys = [];


            if (!$order->getId()){
                return [false, 'No order with id: '. $orderNo . ' found'];
            }

            $orderItems = $order->getAllVisibleItems();

                foreach ($orderItems as $orderItem) {
                    $qtys[$orderItem->getId()] = $orderItem->getQtyOrdered();
                }




            /** creating invoice */
            if($order->canInvoice()) {
                $invoice = null;

                $canCapturePartial = false;

                if ($this->invoiceViaApi()) {
                    $invoiceId = $this->magentoApiConnector->createInvoice($order);

                    if ($invoiceId > 1) {
                        $invoice = $this->invoiceRepository->get($invoiceId);
                        $order = $invoice->getOrder();
                    }

                } else {

                    if ($canCapturePartial) {
                        $itemsArray = $qtys;
                        $invoice = $this->_invoiceService->prepareInvoice($order, $itemsArray);
                    }else{
                        $invoice = $this->_invoiceService->prepareInvoice($order);
                    }


                    $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
                    $invoice->register();
                    $transactionSave = $this->_transaction->addObject(
                        $invoice
                    )->addObject(
                        $invoice->getOrder()
                    );
                    $transactionSave->save();
                }

                //$this->invoiceSender->send($invoice);
                //send notification code
                if ($invoice) {
                    $order->addStatusHistoryComment(
                        __('Notified customer about invoice #%1.', $invoice->getId())
                    )->setIsCustomerNotified(true)
                        ->save();


                    print_r("Created invoice for order: " . $order->getIncrementId() . "\n");
                }

            }

            /* creating shipping item */
            if ($order->hasInvoices() == true) {
                if ($order->canShip() == true) {

                    /*$shipment = $this->shipmentFactory->create($order);
                    // save the newly created shipment

                    $this->shipmentRepository->save($shipment);
                    // send shipping confirmation e-mail to customer
                    $this->shipmentNotifier->notify($shipment);*/

                    // Initialize the order shipment object
                    $convertOrder = $this->objectManager->create('Magento\Sales\Model\Convert\Order');
                    $shipment = $convertOrder->toShipment($order);

// Loop through order items
                    foreach ($order->getAllItems() AS $orderItem) {
                        // Check if order item has qty to ship or is virtual
                        if (! $orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                            continue;
                        }

                        $qtyShipped = $orderItem->getQtyToShip();

                        // Create shipment item with qty
                        $shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);

                        // Add shipment item to shipment
                        $shipment->addItem($shipmentItem);
                    }

// Register shipment
                    $shipment->register();

                    $shipment->getOrder()->setIsInProcess(true);

                    try {
                        // Save created shipment and order

                        $shipment->getExtensionAttributes()->setSourceCode('default');

                        $shipment->save();
                        $shipment->getOrder()->setShipped(1)->save();



                        if (isset($shipmentData->orderDHLTrackingID) && $shipmentData->orderDHLTrackingID) {
                            $shipment = $this->addTrack($shipment, 'custom', $order->getShippingDescription(), $shipmentData->orderDHLTrackingID);
                        }

                        if (isset($shipmentData->orderPostNordTrackingID) && $shipmentData->orderPostNordTrackingID) {
                            $shipment = $this->addTrack($shipment, 'custom', $order->getShippingDescription(), $shipmentData->orderPostNordTrackingID);
                        }
                        $shipment->save();

                        $this->shipmentNotifier->notify($shipment);


                        print_r("Created shipment for order: " . $order->getIncrementId() . "\n");
                    } catch (\Exception $e) {
                        return [false, 'Error in create shipment: '. $e->getMessage()];
                    }


                } else {
                    return [false, 'canShip = false for order:' . $order->getIncrementId()];
                }
            } else {
                return [false, 'Cannot do shipment for order:' . $order->getIncrementId() . ' It has no invoice.'];
                //throw new \Magento\Framework\Exception\LocalizedException('Cannot do shipment for the order. It has no invoice.');
            }

            return array(true);

        }catch (\Exception $e){
            return array(false, $e->getMessage());
        }

    }

    protected function addTrack($shipment, $carrierCode, $description, $trackingNumber)
    {
        /** Creating Tracking */
        /** @var Track $track */
        $track = $this->_trackFactory->create();
        $track->setCarrierCode($carrierCode);
        $track->setDescription($description);
        $track->setTrackNumber($trackingNumber);
        return $shipment->addTrack($track);
    }
}
