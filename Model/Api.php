<?php
namespace Oddny\PyramidConnector\Model;


/**
 * Copyright (c) 2015-2016 Oddny AB
 *
 * Oddny reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of Oddny, except as provided by licence. A licence
 * under Oddny's rights in the Program may be available directly from
 * Oddny.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    Oddny
 * @package     Oddny_Module
 * @copyright   Copyright (c) 2015-2016 Oddny AB
 */
class Api extends \Magento\Framework\Model\AbstractModel
{
    private $_stockMethod = "stock";
    private $_orderMethod = "orders";
    private $_exportOrderMethod = "order";
    private $_productMethod = "products";
    private $_shipmentMethod = "shipments";
    private $_priceMethod = "prices";

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \GuzzleHttp\Client
     */
    protected $guzzle;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
    }
    private function _getApiUrl()
    {
        return $this->scopeConfig->getValue('pyramidconnector/general/pyramid_api_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    private function _getApiKey()
    {
        return $this->scopeConfig->getValue('pyramidconnector/general/pyramid_api_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }


    public function getStock($sku = '')
    {

    }

    public function exportOrder($orderData)
    {

        $client = new \SoapClient($this->_getApiUrl(), ['trace' => 1]);
        $success = false;
        $result = null;

        try{
            $result = $client->CreateWebOrder($orderData);
        } catch (\Exception $e) {
            $success = false;
            $errorMsg = $e->getMessage();
            $requestRaw = $client->__getLastRequest();
            $this->logger->log(\Monolog\Logger::ERROR, 'exception: '.$e->getMessage());
            $this->logger->log(\Monolog\Logger::ERROR, 'Request: ');
            $this->logger->log(\Monolog\Logger::ERROR, $requestRaw);
            $this->logger->log(\Monolog\Logger::ERROR, 'Response: ');
            $this->logger->log(\Monolog\Logger::ERROR, $result);
        }

        if (isset($result->weborderGotID)) {
            return ['success' => true, 'externalOrderId' => $result->weborderGotID];
        }elseif (isset($errorMsg)) {
            return ['success' => false, 'errorMsg' => $errorMsg];
        }

        return ['success' => false];

    }

    public function getShipment($orderId)
    {

        $client = new \SoapClient($this->_getApiUrl(), ['trace' => 1]);
        $success = false;
        $result = null;

        try{
            $result = $client->GetWebOrderStatus(['orderID' => $orderId]);
        } catch (\Exception $e) {
            $this->logger->log(\Monolog\Logger::ERROR, 'exception: '.$e->getMessage());
            $this->logger->log(\Monolog\Logger::ERROR, 'Response: ');
            $this->logger->log(\Monolog\Logger::ERROR, $result);
        }

        if ($result && isset($result->orderStatusID) && ($result->orderStatusID == 'L' || $result->orderStatusID == 'A')) {
            return [$result];
        }

        return [];
    }
}
