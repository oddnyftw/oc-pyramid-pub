<?php
/**
 * Copyright (c) 2015-2015 Oddny AB
 *
 * Oddny reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of Oddny, except as provided by licence. A licence
 * under Oddny's rights in the Program may be available directly from
 * Oddny.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    Oddny
 * @package     Oddny_Module
 * @copyright   Copyright (c) 2015-2015 Oddny AB
 */

namespace Oddny\PyramidConnector\Helper;
use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /**
     * @var \Magento\Catalog\Api\ProductAttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var array
     */
    protected $attributeValues;

    /**
     * @var \Magento\Eav\Api\AttributeOptionManagementInterface
     */
    protected $attributeOptionManagement;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionLabelInterface
     */
    protected $attributeOptionLabel;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionInterface
     */
    protected $attributeOption;

    protected $loadedProducts = [];

    protected $productRepository;

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Eav\Api\Data\AttributeOptionLabelInterface $attributeOptionLabelManagement
     * @param \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Eav\Api\Data\AttributeOptionLabelInterface $attributeOptionLabel,
        \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement,
        \Magento\Eav\Api\Data\AttributeOptionInterface $attributeOption,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository

    ) {
        parent::__construct($context);

        $this->attributeOptionManagement        = $attributeOptionManagement;
        $this->attributeOptionLabel             = $attributeOptionLabel;
        $this->attributeOption                  = $attributeOption;
        $this->productRepository                = $productRepository;

    }

    public function productExist($sku)
    {
        return $this->loadProductBySku($sku) ? true : false;
    }

    public function loadProductBySku($sku)
    {
        if (isset($this->loadedProducts[$sku])) {
            return $this->loadedProducts[$sku];
        }

        try {
            $prod = $this->productRepository->get($sku);
        }catch (\Magento\Framework\Exception\NoSuchEntityException $e){
            return null;
        }
        if ($prod) {
            $this->loadedProducts[$sku] = $prod;
            return $prod;
        }

        return null;
    }



    public function getAttributeOptions()
    {
        $result = [];
        $options = $this->attributeOptionManagement->getItems('catalog_product', 'color');
        foreach ($options as $option) {
            if ($option->getValue() != "") {
                $result[$option->getValue()] = $option->getLabel();
            }
        }

        $newOption = $this->attributeOption->setLabel('test');
        $this->attributeOptionManagement->add('catalog_product', 'color', $newOption);

        return $result;
    }


    public function createAttributeOptions($attributeCode, $attributeOptions)
    {
        $result = [];
        $newOptions = [];
        $options = $this->attributeOptionManagement->getItems('catalog_product', $attributeCode);
        foreach ($options as $option) {
            if ($option->getValue() != "") {
                $result[$option->getValue()] = $option->getLabel();
            }
        }

        $newOption = null;
        foreach ($attributeOptions as $optionToCreate) {
            if (!in_array($optionToCreate, $result)) {
                $newOption = $this->attributeOption->setLabel($optionToCreate);
                $this->attributeOptionManagement->add('catalog_product', $attributeCode, $newOption);
                $newOptions[] = $optionToCreate;
            }
        }



        return $newOptions;
    }

    /**
     * Get attribute by code.
     *
     * @param string $attributeCode
     * @return \Magento\Catalog\Api\Data\ProductAttributeInterface
     */
    public function getAttribute($attributeCode)
    {
        return $this->attributeRepository->get($attributeCode);
    }

    /**
     * Find the ID of an option matching $label, if any.
     *
     * @param string $attributeCode Attribute code
     * @param string $label Label to find
     * @param bool $force If true, will fetch the options even if they're already cached.
     * @return int|false
     */
    public function getOptionId($attributeCode, $label, $force = false)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute */
        $attribute = $this->getAttribute($attributeCode);

        // Build option array if necessary
        if ($force === true || !isset($this->attributeValues[ $attribute->getAttributeId() ])) {
            $this->attributeValues[ $attribute->getAttributeId() ] = [];

            // We have to generate a new sourceModel instance each time through to prevent it from
            // referencing its _options cache. No other way to get it to pick up newly-added values.

            /** @var \Magento\Eav\Model\Entity\Attribute\Source\Table $sourceModel */
            $sourceModel = $this->tableFactory->create();
            $sourceModel->setAttribute($attribute);

            foreach ($sourceModel->getAllOptions() as $option) {
                $this->attributeValues[ $attribute->getAttributeId() ][ $option['label'] ] = $option['value'];
            }
        }

        // Return option ID if exists
        if (isset($this->attributeValues[ $attribute->getAttributeId() ][ $label ])) {
            return $this->attributeValues[ $attribute->getAttributeId() ][ $label ];
        }

        // Return false if does not exist
        return false;
    }
}
